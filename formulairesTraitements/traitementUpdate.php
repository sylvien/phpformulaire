<?php

session_start();
    include '../config/config.php';

    try{ 
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

    } catch (Exception $ex) {
        echo $ex->getMessage();
        die();
    }

   $sql = "UPDATE serie SET titre = :titre, createur = :createur, genre = :genre WHERE id = :id";

   $statement = $bdd->prepare($sql);
   $statement->bindParam(":titre", $titre);
   $statement->bindParam(":createur", $createur);
   $statement->bindParam(":genre", $genre);
   $statement->bindParam(":id", $id);
   $titre = $_POST["titre"];
   $createur = $_POST["createur"];
   $genre = $_POST["genre"];
   $id = $_POST["id"];

   if($statement->execute()){
       header("location: ./traitementTous.php");
   }else{
       echo $statement->errorInfo();
   }

?>

<?php
session_start();
include '../config/config.php';

try{ 
    $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

} catch (Exception $ex) {
    echo $ex->getMessage();
    die();
}

//        var_dump($bdd);

$sql = "INSERT INTO serie (titre, createur, genre) VALUES (:titre, :createur, :genre)";

$statement = $bdd->prepare($sql);
$titre = $_POST["titre"];
$createur = $_POST["createur"];
$genre = $_POST["genre"];
$statement->bindValue(":titre", $titre);
$statement->bindValue(":createur", $createur);
$statement->bindValue(":genre", $genre);
if($statement->execute()){
    header("location: ./traitementTous.php");
    // echo 'ajout réussi';
}else{
    echo 'ajout echoué';
    $statement->errorInfo();
}


?>


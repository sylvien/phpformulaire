<?php

session_start();
    include '../config/config.php';

    try{ 
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

    } catch (Exception $ex) {
        echo $ex->getMessage();
        die();
    }

    $sql = "SELECT * FROM serie WHERE LOWER(createur) LIKE :createur";
    $createur = strtolower("%" . $_POST['createur'] . "%");

    $statement = $bdd->prepare($sql);
    $statement->bindValue(":createur", $createur);
    $statement->execute();
    $tableau = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    $_SESSION["afficher"] = $tableau;

    header("location: ../general.php");

?>
<?php

session_start();
    include '../config/config.php';

    try{ 
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

    } catch (Exception $ex) {
        echo $ex->getMessage();
        die();
    }

    $sql = "SELECT * FROM serie";

    $statement = $bdd->prepare($sql);
    $statement->execute();
    $tableau = $statement->fetchAll(PDO::FETCH_ASSOC);
    
    $_SESSION["afficher"] = $tableau;

    header("location: ../general.php");

?>
<?php

session_start();
    include '../config/config.php';

    try{ 
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

    } catch (Exception $ex) {
        echo $ex->getMessage();
        die();
    }

    $sql = "DELETE FROM serie WHERE id = :id";

    $statement = $bdd->prepare($sql);
    $statement->bindParam("id", $id);
    $id = $_POST["id"];

    if($statement->execute()){
        header("location: ./traitementTous.php");
    }else{
        $statement->errorInfo();
    }


?>
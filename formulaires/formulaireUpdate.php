<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>seriesBD | CHANGER</title>
</head>
<body>
    <?php
    session_start();
    include '../config/config.php';

    try{ 
        $bdd = new PDO(DBDRIVER.':host='.DBHOST.';port='.DBPORT.';dbname='.DBNAME.';charset='.DBCHARSET,DBUSER,DBPASS);

    } catch (Exception $ex) {
        echo $ex->getMessage();
        die();
    }

    $modif = $_POST["id"];

    $sql = "SELECT * FROM serie WHERE id = :id";

    $statement = $bdd->prepare($sql);
    $statement->bindValue(":id", $modif);
    $statement->execute();

    $changer = $statement->fetchAll(PDO::FETCH_ASSOC);

    foreach ($changer as $serie) {
        echo '<form action="../formulairesTraitements/traitementUpdate.php" method="post">';
            echo '<input type="hidden" name="id" value="' . $serie["id"] . '"> ';
            echo 'titre <input type="text" name="titre" value="' . $serie["titre"] . '"> ';
            echo 'createur <input type="text " name="createur" value="' . $serie["createur"] . '"> ';
            echo 'genre <input type="text" name="genre" value="' . $serie["genre"] . '"> ';
            echo '<button type="submit">CHANGE</button>';
        echo '</form>';
    }
    ?>

</body>
</html>
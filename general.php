<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="./style/reset.css">
    <link rel="stylesheet" href="./style/style.css">
    <title>Formulaires & Sessions</title>
</head>
<body>

    <section>
    <?php
    session_start();
    echo '<nav>';
        echo '<ul class="choixul">';
            echo '<a href="./formulairesTraitements/traitementTous.php"><li class="choixli noir">tous</li></a>';
            echo '<a href="./formulaires/formulaireRechercheTitre.html"><li class="choixli rouge">par titre</li></a>';
            echo '<a href="./formulaires/formulaireRechercheGenre.html"><li class="choixli jaune">par genre</li></a>';
            echo '<a href="./formulaires/formulaireRechercheCreateur.html"><li class="choixli bleu">par createur</li></a>';
        echo '</ul>';
    echo '</nav>';

    // if(isset($_SESSION["ajouter"]) && $_SESSION["ajouter"] != false){
    //     include './formulaires/formAjouterSerie.html';
    // }
    if(isset($_SESSION["afficher"]) && $_SESSION["afficher"] != false){
        include './afficher.php';
    }
    
    ?>
    </section>

    <a class="add" href="./formulaires/formulaireAjouterSerie.html">+</a>
    
</body>
</html>